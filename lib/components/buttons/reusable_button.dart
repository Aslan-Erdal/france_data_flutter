import 'package:flutter/material.dart';
import 'package:flutter_examen1/constants/color_constants.dart';
import 'package:flutter_examen1/router/routes.dart';
import 'package:go_router/go_router.dart';

class ReusableElevetedButton extends StatelessWidget {
  const ReusableElevetedButton({super.key, required this.title, required this.icon});

  final String title;
  final IconData icon;
  // d'autres propriétés peuvent être ajoutés...

  @override
  Widget build(BuildContext context) {
    return ElevatedButton.icon(
          icon: Icon(
            icon,
            color: elevatedButtonIconColor,
            size: 24.0,
          ),
          label: Text(title, 
          style: const TextStyle(
            color: blackColor,
            fontSize: 17,
            fontWeight: FontWeight.bold
          ),
          ),
          onPressed: () {
            context.go(Routes.regions);
          },
        );
  }
}