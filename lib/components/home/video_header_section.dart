import 'package:flutter/material.dart';
import 'package:flutter_examen1/components/home/video_hero.dart';
import 'package:flutter_examen1/constants/color_constants.dart';
import 'package:flutter_examen1/constants/text.constants.dart';

class HeaderVideoSection extends StatelessWidget {
  const HeaderVideoSection({super.key});

  @override
  Widget build(BuildContext context) {
    return const Stack(
      children: <Widget>[
        VideoHero(),
        Center(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 80,
              ),
              Text(
                headerTitle,
                style: TextStyle(
                    fontSize: 22,
                    color: greyColor,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 3
                    ),
              ),
              Text(
                headerSubTitle,
                style: TextStyle(
                    fontSize: 18,
                    color: greyColor,
                    letterSpacing: 2
                    ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
