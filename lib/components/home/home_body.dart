import 'package:flutter/material.dart';
import 'package:flutter_examen1/components/buttons/reusable_button.dart';
import 'package:flutter_examen1/components/home/video_header_section.dart';
import 'package:flutter_examen1/constants/color_constants.dart';
import 'package:flutter_examen1/constants/text.constants.dart';

class MyHome extends StatelessWidget {
  const MyHome({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: const <Widget>[
        HeaderVideoSection(),
        SizedBox(
          width: double.infinity,
          child: DecoratedBox(
            decoration: BoxDecoration(color: blueColor),
            child: Padding(
              padding: EdgeInsets.only(left: 10, top: 6, bottom: 6),
              child: Text(
                titleRegions,
                style: TextStyle(
                    fontSize: 20.0,
                    color: whiteColor,
                    backgroundColor: blueColor,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.all(12.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Text(regionsDescription),
              SizedBox(
                height: 55,
              ),
              ReusableElevetedButton(
                title: 'Rechercher par Région',
                icon: Icons.search,
              ),
              SizedBox(
                height: 55,
              ),
              Text(
                regionsDescription,
                style: TextStyle(
                  color: blackColor,
                  fontSize: 10,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
