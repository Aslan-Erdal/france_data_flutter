import 'package:flutter/material.dart';

class MyRegions extends StatelessWidget {
  const MyRegions({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text('Régions',
      style: TextStyle(
        fontSize: 24,
        fontWeight: FontWeight.bold,
        shadows: <Shadow>[
      Shadow(
        offset: Offset(-10.0, 15.0),
        blurRadius: 10.0,
        color: Color.fromARGB(255, 0, 0, 0),
      ),
    ],
      ),
      ),
    );
  }
}