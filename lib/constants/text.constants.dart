// Home texts constants
const headerTitle = "France Data";
const headerSubTitle = "Atlas des données de France";
const titleRegions = "I. Nos Régions";
const regionsDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.  Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";


// Menu texts constants
const menuTitle = "France Data";
const menuSubTitle = "Atlas de Données Françaises";