import 'package:flutter/material.dart';

const blueAccent = Colors.blueAccent;
const whiteColor = Colors.white;
const blackColor = Colors.black;
const blueColor = Colors.blue;
const greyColor = Colors.grey;

// ElevatedButton
const elevatedButtonIconColor = Colors.blue;

// Menu Colors
const drawerHeaderColor = Color.fromARGB(255, 55, 97, 134);