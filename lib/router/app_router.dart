import 'package:flutter_examen1/pages/home_page.dart';
import 'package:flutter_examen1/pages/parametres_page.dart';
import 'package:flutter_examen1/pages/regions_page.dart';
import 'package:flutter_examen1/router/routes.dart';
import 'package:go_router/go_router.dart';

class AppRouter {
  static final GoRouter router = GoRouter(
    debugLogDiagnostics: true,
    initialLocation: Routes.accueil,
    routes: [
      GoRoute(
        path: Routes.accueil,
        builder: (context, state) => const HomePage(title: 'France Data',),
      ),
      GoRoute(
        path: Routes.regions,
        builder: (context, state) => const RegionsPage(title: 'Recherche Par Region ',),
      ),
      GoRoute(
        path: Routes.parametres,
        builder: (context, state) => const ParametresPage(title: 'Parametres',),
      ),
    ],
  );
}