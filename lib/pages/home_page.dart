import 'package:flutter/material.dart';
import 'package:flutter_examen1/components/home/home_body.dart';
import 'package:flutter_examen1/pages/partials/my_custom_scaffold.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return MyCustomScaffold(title: title, child: const MyHome());
  }
}
