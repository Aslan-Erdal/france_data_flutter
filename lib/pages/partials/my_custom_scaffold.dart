import 'package:flutter/material.dart';
import 'package:flutter_examen1/pages/partials/menu/menu.dart';
import 'package:flutter_examen1/constants/color_constants.dart';

class MyCustomScaffold extends StatelessWidget {
  const MyCustomScaffold({super.key, required this.child, required this.title});

  final Widget child;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(
          title,
          style: const TextStyle(
            color: whiteColor,
          ),
        ),
        iconTheme: const IconThemeData(color: whiteColor),
      ),
      drawer: const MyDrawerWidget(),
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("web/assets/img/karolina.jpg"),
            fit: BoxFit.cover
            ),
        ),
        child: child), 
    );
  }
}