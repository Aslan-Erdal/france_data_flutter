import 'package:flutter/material.dart';
import 'package:flutter_examen1/router/routes.dart';
import 'package:go_router/go_router.dart';

class MenuFooter extends StatelessWidget {
  const MenuFooter({super.key});

  @override
  Widget build(BuildContext context) {
    // Will be refactor later for displaying menu dynamically if I have time!
    return ConstrainedBox(
      constraints: const BoxConstraints(
        minHeight: 50,
        maxHeight: 615,
      ),
      child: DecoratedBox(
        decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage("web/assets/img/karolina.jpg"),
              fit: BoxFit.cover),
        ),
        child: Column(
          children: [
            ListTile(
              onTap: () => context.go(Routes.accueil),
              leading: const Icon(Icons.home),
              title: const Text(
                'Accueil',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            ListTile(
              onTap: () => context.go(Routes.regions),
              leading: const Icon(Icons.map),
              title: const Text(
                'Régions',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            ListTile(
              onTap: () => context.go(Routes.parametres),
              leading: const Icon(Icons.settings),
              title: const Text(
                'Paramètres',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
