import 'package:flutter/material.dart';
import 'package:flutter_examen1/pages/partials/menu/menu_footer/menu_footer.dart';
import 'package:flutter_examen1/pages/partials/menu/menu_header/menu_header.dart';
// import 'package:flutter_svg/svg.dart';

class MyDrawerWidget extends StatelessWidget {
  const MyDrawerWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
        // if you want to set background color/opacity...
        // backgroundColor: Color.fromARGB(220, 255, 255, 255),
        width: 273,
        child: ListView(
          padding: const EdgeInsets.all(0),
          children: const <Widget>[
            MenuHeader(),
            MenuFooter(),
          ],
        ));
  }
}
