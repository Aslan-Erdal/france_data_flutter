import 'package:flutter/material.dart';
import 'package:flutter_examen1/constants/color_constants.dart';
import 'package:flutter_examen1/constants/text.constants.dart';

class MenuHeader extends StatelessWidget {
  const MenuHeader({super.key});

  @override
  Widget build(BuildContext context) {
    return const DrawerHeader(
        margin: EdgeInsets.all(0),
        decoration: BoxDecoration(
          color: drawerHeaderColor,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              menuTitle,
              style: TextStyle(
                color: whiteColor,
                fontSize: 24,
              ),
            ),
            Text(
              menuSubTitle,
              style: TextStyle(
                color: whiteColor,
                fontSize: 14,
              ),
            )
          ],
        ));
  }
}
