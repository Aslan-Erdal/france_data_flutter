import 'package:flutter/material.dart';
import 'package:flutter_examen1/components/regions/regions_body.dart';
import 'package:flutter_examen1/pages/partials/my_custom_scaffold.dart';

class RegionsPage extends StatelessWidget {
  const RegionsPage({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return MyCustomScaffold(title: title, child: const MyRegions());
  }
}