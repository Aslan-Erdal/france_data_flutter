import 'package:flutter/material.dart';
import 'package:flutter_examen1/components/parametres/parametres_body.dart';
import 'package:flutter_examen1/pages/partials/my_custom_scaffold.dart';

class ParametresPage extends StatelessWidget {
  const ParametresPage({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return MyCustomScaffold(title: title, child: const MyParametres());
  }
}